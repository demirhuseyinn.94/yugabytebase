Role Name
=========

yugabase

This role is responsible for completing pre requirements in order to install and start yugabyte database. It can be used for the following reasons

1. Installing the yugabyte base packages
2. Configuring the yugabyte post installation settings

Role Dependicies
=========

Supported Operating Systems

* CentOS 7.X

Requirements
=========

* ansible>=2.9.3

Installation
=========

``` bash
git clone https://gitlab.com/demirhuseyinn.94/yugabytebase.git
``` 

``` bash
ansible-galaxy install -r requirements.yml
``` 

Example Playbook
=========

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
``` yaml
---
- hosts: localhost
  connection: local
  roles:
    - yugabase
```

Author Information
=========

Hüseyin Demir
